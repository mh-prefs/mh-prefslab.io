import constants from '@/constants';

export default bggUserName => `${constants.BGG_API_URL}/user/${bggUserName}`;
