/**
 * @param {any[]} geeklist
 */
export default (geeklist) => {
  const authors = new Map();
  geeklist.forEach((listItem) => {
    if (listItem.authors) {
      listItem.authors.forEach(author => authors.has(author.id) || authors.set(author.id, author));
    }
    if (listItem.subitems) {
      listItem.subitems.forEach((subitem) => {
        if (subitem.authors) {
          subitem.authors.forEach(author => authors.has(author.id) || authors.set(author.id, author));
        }
      });
    }
  });
  return Array.from(authors.values());
};
