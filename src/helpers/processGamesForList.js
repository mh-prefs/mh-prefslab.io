import bbCodeParser from 'js-bbcode-parser';

export default games => games.map((game) => {
  const gameNew = { ...game };
  gameNew.text = bbCodeParser.parse(game.text);
  return gameNew;
});
