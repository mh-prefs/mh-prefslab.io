export default games => games.reduce((namedGroups, game) => {
  const initName = game.name + game.subitems.reduce((prev, subitem) => `${prev}+${subitem.name}`, '');
  const groupName = `%${initName.replace(/[^a-z0-9_]+/ig, '_').replace(/_{2,}/ig, '_').replace(/_+$/i, '').toUpperCase()}`;
  const existingGroup = namedGroups.find(group => group.name === groupName);
  if (existingGroup) {
    existingGroup.items.push(game.number);
  } else {
    namedGroups.push({ name: groupName, id: game.id, gameId: game.id, items: [game.number] });
  }

  return namedGroups;
}, []);
