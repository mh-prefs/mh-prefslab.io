export const filterType = {
  SWITCH: 'switch',
  TEXT: 'text',
  SINGLE_CHOICE: 'singleChoice',
  MULTI_CHOICE: 'multiChoice',
  NUMBER: 'number',
};

const params = new URLSearchParams(window.location.search);
const thumbsEncoded = params.get('thumbs') ? params.get('thumbs').split(',') : [];
let itemsWithThumbs = [];
if (thumbsEncoded.length) {
  const firstThumb = parseInt(thumbsEncoded.shift(), 32);
  itemsWithThumbs = [firstThumb, ...thumbsEncoded.map(thumbEnc => parseInt(thumbEnc, 32) + firstThumb)];
}

export default {
  noUserGames: {
    name: 'noUserGames',
    label: 'Bez wystawionych przeze mnie',
    type: filterType.SWITCH,
    value: null,
    reducer: bggUser => game => game.bggUser.toLowerCase() !== bggUser.toLowerCase(),
  },
  noUserOwnedGames: {
    name: 'noUserOwnedGames',
    label: 'Bez posiadanych przeze mnie',
    type: filterType.SWITCH,
    value: null,
    reducer: userOwned => game => !userOwned.includes(game.gameId) || (game.subitems.length && !game.subitems.some(subitem => userOwned.includes(subitem.gameId))),
  },
  noUserPrevOwnedGames: {
    name: 'noUserPrevOwnedGames',
    label: 'Bez posiadanych dawniej przeze mnie',
    type: filterType.SWITCH,
    value: null,
    reducer: userPrevOwned => game => !userPrevOwned.includes(game.gameId) || (game.subitems.length && !game.subitems.some(subitem => userPrevOwned.includes(subitem.gameId))),
  },
  wishlistOnly: {
    name: 'wishlistOnly',
    label: 'Tylko gry z mojej wishlisty',
    type: filterType.SWITCH,
    value: null,
    reducer: userWishlist => game => userWishlist.includes(game.gameId) || game.subitems.some(subitem => userWishlist.includes(subitem.gameId)),
  },
  wantedOnly: {
    name: 'wantedOnly',
    label: 'Tylko gry oznaczone "Want in Trade"',
    type: filterType.SWITCH,
    value: null,
    reducer: userWanted => game => userWanted.includes(game.gameId) || game.subitems.some(subitem => userWanted.includes(subitem.gameId)),
  },
  name: {
    name: 'name',
    label: 'Nazwa gry',
    type: filterType.TEXT,
    reducer: namePart => game => game.name.toLowerCase().includes(namePart.toLowerCase()) || game.subitems.some(item => item.name.toLowerCase().includes(namePart.toLowerCase())),
  },
  minBggAvgRating: {
    name: 'minBggAvgRating',
    label: 'Minimalna średnia ocena',
    type: filterType.NUMBER,
    reducer: min => game => (game.avgRating && game.avgRating >= min) || game.subitems.some(item => item.avgRating && item.avgRating >= min),
  },
  bggMainRank: {
    name: 'bggMainRank',
    label: 'Max pozycja w rankingu',
    type: filterType.NUMBER,
    reducer: max => game => (game.mainrank && game.mainrank <= max) || game.subitems.some(item => item.mainrank && item.mainrank <= max),
  },
  minBggWeight: {
    name: 'minBggWeight',
    label: 'Minimalna ciężkość (wg BGG)',
    type: filterType.NUMBER,
    reducer: min => game => (game.weight && game.weight >= min) || game.subitems.some(item => item.weight && item.weight >= min),
  },
  maxBggWeight: {
    name: 'maxBggWeight',
    label: 'Maksymalna ciężkość (wg BGG)',
    type: filterType.NUMBER,
    reducer: max => game => (game.weight && game.weight <= max) || game.subitems.some(item => item.weight && item.weight <= max),
  },
  bggUser: {
    name: 'bggUser',
    label: 'Wystawiający (nick BGG)',
    type: filterType.MULTI_CHOICE,
    options: [],
    reducer: users => game => users.includes(game.bggUser),
  },
  releaseYear: {
    name: 'releaseYear',
    label: 'Rok wydania (oryginału, wg BGG)',
    type: filterType.NUMBER,
    reducer: year => game => game.yearpublished === Number(year) || game.subitems.some(item => item.yearpublished === Number(year)),
  },
  authors: {
    name: 'authors',
    label: 'Autorzy',
    type: filterType.MULTI_CHOICE,
    options: [],
    reducer: authors => game =>
      (game.authors && game.authors.some(author => authors.some(aut => aut.id === author.id)))
      || game.subitems.some(item => item.authors && item.authors.some(author => authors.some(aut => aut.id === author.id))),
  },
  excludeUsers: {
    name: 'excludeUsers',
    label: 'Wykluczeni wystawiający (nick BGG)',
    type: filterType.MULTI_CHOICE,
    options: [],
    reducer: users => game => !users.includes(game.bggUser),
  },
  ...(itemsWithThumbs.length && {
    withUserThumbs: {
      name: 'withUserThumbs',
      label: 'Z moimi kciukami (thx Planszove!)',
      type: filterType.SWITCH,
      value: itemsWithThumbs,
      reducer: itemsThumbs => game => itemsThumbs.includes(game.itemId),
    },
  }),
};
