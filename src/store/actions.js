import bggUserUrl from '@/helpers/bggUserUrl';
import extractNamedGroups from '@/helpers/extractNamedGroups';
import router from '@/router';
import { bgg2json } from 'bgg2json';
import getGamesAuthors from '@/helpers/getGamesAuthors';
import getGamesOwners from '@/helpers/getGamesOwners';

export default {

  async init({ state, commit, dispatch }) {
    const isStale = state.mhId !== state.lastMhId;
    dispatch(isStale ? 'resetState' : 'getGeeklistItems');
    commit('setLastMhId', state.mhId);
  },

  async resetState({ commit, dispatch }) {
    commit('resetState');
    await dispatch('getGeeklistItems');
    router.push('/get-user');
  },

  async getGeeklistItems({ state, commit, dispatch }) {
    const response = await fetch(`/static/lists/${state.mhId}/list.json`);
    const games = await response.json();
    const namedGroups = extractNamedGroups(games);
    commit('setGeeklistItems', games);
    commit('setNamedGroups', namedGroups);
    dispatch('extractGamesAuthors');
    dispatch('extractGaesOwners');
  },

  extractGamesAuthors({ state, commit }) {
    const authors = getGamesAuthors(state.geeklistItems);
    commit('setAuthors', authors);
  },

  extractGaesOwners({ state, commit }) {
    const owners = getGamesOwners(state.geeklistItems);
    commit('setOwners', owners);
  },

  async getBggUser({ commit, dispatch }, bggUser) {
    await fetch(bggUserUrl(bggUser));
    commit('setBggUser', bggUser);
    dispatch('initUserPrefs');
    try {
      const { data } = await bgg2json('collection', { username: bggUser });
      const extractId = item => parseInt(item.objectid, 10);
      const statusPredicate = statusName => item => item.status[statusName] === '1';
      const wishlistedGames = (data.item || []).filter(statusPredicate('wishlist')).map(extractId);
      const wantedGames = (data.item || []).filter(statusPredicate('want')).map(extractId);
      const ownedGames = (data.item || []).filter(statusPredicate('own')).map(extractId);
      const prevownedGames = (data.item || []).filter(statusPredicate('prevowned')).map(extractId);
      commit('setUserGames', { wishlistedGames, ownedGames, prevownedGames, wantedGames });
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }
  },

  async initUserPrefs({ state, commit, getters }) {
    if (state.userPrefs && state.userPrefs.length) return;
    const userGames = getters.userGames;
    const prefs = userGames.map(userGame => ({
      game: userGame,
      groups: [],
    }));
    commit('setUserPrefs', prefs);
  },

  initUserNamedGroups({ state, commit, getters }) {
    const chosenGamesChanged = state.chosenGames.length !== state.chosenGamesOnGroupsInit.length
      || !state.chosenGames.every(gameNumber =>
        state.chosenGamesOnGroupsInit.includes(gameNumber));
    const userNgLen = state.userNamedGroups.length;
    if (!userNgLen || chosenGamesChanged) {
      commit('setUserNamedGroups', [...getters.chosenGamesAsNamedGroups]);
      commit('setChosenGamesOnGroupsInit', [...state.chosenGames]);
    }
  },

  addUserNamedGroup({ commit }) {
    const id = Date.now();
    commit('addUserNamedGroup', { id, name: `%${id}`, items: [] });
  },

  setFilter({ state, getters, commit }, filter) {
    commit('setFilter', filter);
    if ((state.listCurrentPage - 1) * state.listItemsPerPage > getters.filteredGamesCount) {
      commit('setListCurrentPage', 1);
    }
  },

};
