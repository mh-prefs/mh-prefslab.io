import mhs from '@/mhs.json';
import getDefaultState from './getDefaultState';

export default {
  mhId: mhs.latest,
  authors: [],
  owners: [],
  ...getDefaultState(),
};
