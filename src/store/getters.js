import mhs from '@/mhs.json';
import filters from '@/helpers/filters';

export default {

  isGameChosen: state => game => state.chosenGames.includes(game.number),

  noGameChosen: state => !state.chosenGames.length,

  filteredGames: (state) => {
    let games = [...state.geeklistItems];
    Object.entries(state.activeFilters).forEach(([filterName, value]) => {
      if (filters[filterName]) {
        games = games.filter(filters[filterName].reducer(value));
      }
    });
    return state.listSorting === 'default' ? games : games.sort(({ name: nameA }, { name: nameB }) =>
      nameA.toLowerCase().localeCompare(nameB, undefined, { sensitivity: 'base' }));
  },

  filteredGamesCount: (_state, getters) => getters.filteredGames.length,

  chosenGamesAsNamedGroups: state =>
    state.namedGroups
      .filter(group => group.items.some(gameNumber =>
        state.chosenGames.includes(gameNumber)))
      .map((group) => {
        const newGroup = {
          name: group.name,
          items: group.items.filter(gameNumber => state.chosenGames.includes(gameNumber)),
          gameId: group.gameId,
        };
        return newGroup;
      }),

  geeklistItemByNumber: state => number => state.geeklistItems.find(item => item.number === number),

  geeklistItemUrl: (state, getters) => (number) => {
    const itemId = getters.geeklistItemByNumber(number).itemId;
    return `https://boardgamegeek.com/geeklist/${state.mhId}/item/${itemId}#item${itemId}`;
  },

  userGames: state => state.geeklistItems.filter(item => item.bggUser.toLowerCase() === state.bggUser.toLowerCase()),

  wishlistRetrieved: state => !!state.userWishlist.length && !!state.userOwned.length,

  listCurrentPage: state => state.listCurrentPage,

  listLength: (state, getters) => Math.ceil(getters.filteredGames.length / state.listItemsPerPage),

  gamesPage: (state, getters) => getters.filteredGames.slice((getters.listCurrentPage - 1) * state.listItemsPerPage, getters.listCurrentPage * state.listItemsPerPage),

  nonEmptyGroups: ({ userNamedGroups }) => userNamedGroups.filter(group => !!group.items.length),

  unusedGroups: state => state.userNamedGroups
    .filter(group => !!group.items.length)
    .filter(group => !state.userPrefs.some(pref => pref.groups.includes(group.name))),

  chosenMhName: state => mhs.all.find(mh => mh.id === state.mhId).name,

  gameByNumber: state => gameNumber => state.geeklistItems.find(game => game.number === gameNumber),

  activeFilters: state => state.activeFilters,

};
