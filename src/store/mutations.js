import Vue from 'vue';
import getDefaultState from './getDefaultState';

export default {
  resetState(state) {
    Object.assign(state, getDefaultState());
  },
  setGeeklistItems(state, items) {
    state.geeklistItems = items;
  },
  addChosenGame(state, game) {
    state.chosenGames.push(game.number);
  },
  removeChosenGame(state, game) {
    state.chosenGames = state.chosenGames.filter(gameNumber => gameNumber !== game.number);
  },
  setBggUser(state, bggUser) {
    state.bggUser = bggUser;
  },
  setNamedGroups(state, namedGroups) {
    state.namedGroups = namedGroups;
  },
  setUserNamedGroups(state, groups) {
    state.userNamedGroups = groups;
  },
  addUserNamedGroup(state, group) {
    state.userNamedGroups.push(group);
  },
  changeUserNamedGroupName(state, { groupId, newName }) {
    state.userNamedGroups.find(group => group.id === groupId).name = newName;
  },
  removeUserNamedGroup(state, groupId) {
    state.userNamedGroups = state.userNamedGroups.filter(group => group.id !== groupId);
  },
  setChosenGamesOnGroupsInit(state, games) {
    state.chosenGamesOnGroupsInit = games;
  },
  setUserPrefs(state, prefs) {
    state.userPrefs = prefs;
  },
  toggleUserPref(state, { userGame, group }) {
    const foundPref = state.userPrefs.find(pref => pref.game.number === userGame.number);
    if (foundPref.groups.includes(group.name)) {
      foundPref.groups = foundPref.groups.filter(groupName => groupName !== group.name);
    } else {
      foundPref.groups.push(group.name);
    }
  },
  removeGroupFromPrefs(state, group) {
    state.userPrefs.forEach((pref) => {
      // eslint-disable-next-line no-param-reassign
      pref.groups = pref.groups.filter(g => g !== group.name);
    });
  },
  selectAllGroups(state, userGame) {
    const foundPref = state.userPrefs.find(pref => pref.game.number === userGame.number);
    foundPref.groups = state.userNamedGroups.filter(group => !!group.items.length).map(group => group.name);
  },
  deselectAllGroups({ userPrefs }, userGame) {
    const foundPref = userPrefs.find(pref => pref.game.number === userGame.number);
    foundPref.groups = [];
  },
  setListCurrentPage(state, pageNumber) {
    state.listCurrentPage = pageNumber;
  },
  setListItemsPerPage(state, itemsPerPage) {
    state.listItemsPerPage = itemsPerPage;
    state.listCurrentPage = 1;
  },
  removeFilter(state, filterName) {
    Vue.delete(state.activeFilters, filterName);
  },
  setLastMhId(state, id) {
    state.lastMhId = id;
  },
  setUserGames(state, { wishlistedGames, ownedGames, prevownedGames, wantedGames }) {
    state.userWishlist = wishlistedGames;
    state.userOwned = ownedGames;
    state.userPrevOwned = prevownedGames;
    state.userWanted = wantedGames;
  },
  setFilter(state, filter) {
    Vue.set(state.activeFilters, filter.name, filter.value);
  },
  setAuthors(state, authors) {
    state.authors = authors;
  },
  setOwners(state, owners) {
    state.owners = owners;
  },
  setListSorting(state, sorting) {
    state.listSorting = sorting;
  },
};
