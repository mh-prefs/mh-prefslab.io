import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import state from './state';


Vue.use(Vuex);

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  plugins: [createPersistedState({
    paths: [
      'lastMhId',
      'chosenGames',
      'bggUser',
      'namedGroups',
      'userWishlist',
      'activeFilters',
      'listSorting',
      'userNamedGroups',
      'chosenGamesOnGroupsInit',
      'userPrefs',
      'listCurrentPage',
      'listItemsPerPage',
      'userOwned',
      'userPrevOwned',
      'userWanted',
    ],
  })],
});
