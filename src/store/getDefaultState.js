export default () => ({
  geeklistItems: [],
  lastMhId: 0,
  chosenGames: [],
  bggUser: null,
  namedGroups: [],
  userWishlist: [],
  activeFilters: {},
  listSorting: 'default',
  userNamedGroups: [],
  chosenGamesOnGroupsInit: [],
  userPrefs: null,
  listCurrentPage: 1,
  listItemsPerPage: 25,
  userOwned: [],
  userPrevOwned: [],
  userWanted: [],
});
