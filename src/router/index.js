import store from '@/store';
import Vue from 'vue';
import Router from 'vue-router';

const MhList = () => import('@/components/MhList');
const Match = () => import('@/components/Match');
const PrefList = () => import('@/components/PrefList');
const GetUser = () => import('@/components/GetUser');
const NamedGroups = () => import('@/components/NamedGroups');

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    { path: '/', component: MhList, meta: { requiresUser: true } },
    { path: '/named-groups', component: NamedGroups, meta: { requiresUser: true } },
    { path: '/match', component: Match, meta: { requiresUser: true } },
    { path: '/pref-list', component: PrefList, meta: { requiresUser: true } },
    { path: '/get-user', component: GetUser },
  ],
});

router.beforeEach((to, _from, next) => {
  if (to.matched.some(record => record.meta.requiresUser) && !store.state.bggUser) {
    next({
      path: '/get-user',
      query: { redirect: to.fullPath },
    });
  } else {
    next();
  }
});

export default router;
