import Vue from 'vue';
import Vuetify from 'vuetify';
import VueClipboard from 'vue-clipboard2';
import 'vuetify/dist/vuetify.css';
import App from './App';
import router from './router';
import store from './store';

Vue.use(Vuetify);
Vue.use(VueClipboard);

const app = new Vue({
  router,
  store,
  components: { App },
  template: '<App/>',
});

app.$mount('#app');
