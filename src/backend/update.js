const mkdirp = require('mkdirp');

const reduceGameData = require('./helpers/reduceGameData');
const writeJsonToFile = require('./helpers/writeJsonToFile');
const testGeeklist = require('./helpers/testGeeklist');
const chooseMh = require('./helpers/chooseMh');
const mapGeeklist = require('./helpers/mapGeeklist');
const fetchGames = require('./helpers/fetchGames');
const findGame = require('./helpers/findGame');
const bggApi = require('./helpers/bggApi');

const update = async () => {
  try {
    const geeklistId = await chooseMh();
    const result = await bggApi(`geeklist/${geeklistId}`, { comments: 1 });
    let geeklist = mapGeeklist(result.geeklist.item);
    try {
      const games = await fetchGames(geeklist);
      geeklist = geeklist.map((listItem) => {
        try {
          const gameData = findGame(games, listItem);
          return gameData ? {
            ...listItem,
            ...reduceGameData(gameData, listItem),
            subitems: listItem.subitems.map((subitem) => {
              const subgameData = findGame(games, subitem);
              return subgameData ? {
                ...subitem,
                ...reduceGameData(subgameData, subitem),
              } : false;
            }).filter(Boolean),
          } : false;
        } catch (e) {
          // eslint-disable-next-line no-console
          console.error(listItem);
          throw e;
        }
      }).filter(Boolean);
      mkdirp.sync(`static/lists/${geeklistId}`);
      writeJsonToFile(`static/lists/${geeklistId}/list.json`, geeklist);
      testGeeklist(geeklist);
      // eslint-disable-next-line no-console
      console.log('All Done!');
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
  }
};

update();
