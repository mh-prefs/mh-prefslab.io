const bbCodeParser = require('js-bbcode-parser').default;

const extractItemsFromComment = (comment) => {
  if (!comment) {
    return [];
  }

  const matches = comment._?.match(/(?:\[(?:thing|gameid|boardgame|boardgameexpansion|boardgameaccessory)=([\d]+)\])/g) || [];
  return matches.map(match => ({
    gameId: parseInt(/^\[(?:thing|gameid|boardgame|boardgameexpansion|boardgameaccessory)=(?<id>[\d]+)\]$/i.exec(match).groups.id, 10),
    text: bbCodeParser.parse(comment._),
  }));
};

const getListItemSubitems = (listItem) => {
  if (Array.isArray(listItem.comment)) {
    return listItem.comment.filter((comment) => comment.username === listItem.username).map(extractItemsFromComment).flat()
  } else {
    return listItem.comment?.username === listItem.username ? extractItemsFromComment(listItem.comment) : []
  }
};

// let flag = true;

// eslint-disable-next-line arrow-body-style
module.exports = originalGeeklist => originalGeeklist.map((geeklistItem, index) => {
  // eslint-disable-next-line no-console, no-unused-expressions
  // (flag && console.log(geeklistItem), flag = false);
  return {
    itemId: parseInt(geeklistItem.id, 10),
    number: index + 1,
    gameId: parseInt(geeklistItem.objectid, 10),
    name: geeklistItem.objectname,
    bggUser: geeklistItem.username,
    text: bbCodeParser.parse(geeklistItem.body),
    subitems: getListItemSubitems(geeklistItem),
    imageId: parseInt(geeklistItem.imageid, 10),
    objectType: geeklistItem.objecttype,
    postDate: geeklistItem.postdate,
    editDate: geeklistItem.editdate,
    thumbs: geeklistItem.thumbs,
  };
});
