const linkProps = {
  ARTISTS: 'boardgameartist',
  AUTHORS: 'boardgamedesigner',
  CATEGORIES: 'boardgamecategory',
  FAMILIES: 'boardgamefamily',
  MECHANICS: 'boardgamemechanic',
  PUBLISHERS: 'boardgamepublisher',
};

const extractLink = (gamedata, linkProp) => {
  if (Array.isArray(gamedata.link)) {
    return gamedata.link.filter(link => link.type === linkProp)
      .map(link => ({ id: parseInt(link.id, 10), name: link.value }));
  }

  if (gamedata.link?.type === linkProp) {
    return [{ id: parseInt(gamedata.link.id, 10), name: gamedata.link.value }];
  }

  return [];
};

const extractMainRank = (gamedata) => {
  if (!(gamedata.statistics.ratings.ranks.rank instanceof Array)) {
    if (!gamedata.statistics.ratings.ranks.rank) {
      return 0;
    }
    return parseInt(gamedata.statistics.ratings.ranks.rank.value, 10);
  }

  const rankObj = gamedata.statistics.ratings.ranks.rank.find(rank => rank.id === 1 || rank.id === '1');
  return rankObj ? parseInt(rankObj.value, 10) : null;
};

const extractName = (gamedata) => {
  if (Array.isArray(gamedata.name)) {
    const primary = gamedata.name.find(({ type }) => type === 'primary').value;
    return primary || gamedata.name[0].value;
  }

  return gamedata.name?.value;
};

const reduceGameData = (gamedata, listItem) => {
  const version = (Array.isArray(gamedata.versions?.item) ? gamedata.versions.item : [gamedata.versions?.item]).filter(Boolean).find(ver => ver.image?.endsWith(`pic${listItem.imageId}.jpg`));
  return {
    ...listItem,
    id: parseInt(gamedata.id, 10),
    thumbnail: gamedata.thumbnail,
    image: gamedata.image,
    yearpublished: gamedata.yearpublished ? parseInt(gamedata.yearpublished.value, 10) : null,
    minplayers: gamedata.minplayers ? parseInt(gamedata.minplayers.value, 10) : null,
    maxplayers: gamedata.maxplayers ? parseInt(gamedata.maxplayers.value, 10) : null,
    minplaytime: gamedata.minplaytime ? parseInt(gamedata.minplaytime.value, 10) : null,
    maxplaytime: gamedata.maxplaytime ? parseInt(gamedata.maxplaytime.value, 10) : null,
    minage: gamedata.minage ? parseInt(gamedata.minage.value, 10) : null,
    mainrank: extractMainRank(gamedata),
    name: extractName(gamedata) || listItem.name,
    authors: extractLink(gamedata, linkProps.AUTHORS),
    categories: extractLink(gamedata, linkProps.CATEGORIES),
    mechanics: extractLink(gamedata, linkProps.MECHANICS),
    publishers: extractLink(gamedata, linkProps.PUBLISHERS),
    artists: extractLink(gamedata, linkProps.ARTISTS),
    families: extractLink(gamedata, linkProps.FAMILIES),
    type: gamedata.type,
    weight: parseFloat(gamedata.statistics.ratings.averageweight.value),
    avgRating: parseFloat(gamedata.statistics.ratings.average.value),
    version,
  };
};

module.exports = reduceGameData;
