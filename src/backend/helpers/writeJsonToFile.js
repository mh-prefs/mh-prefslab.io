const fs = require('fs');

const writeJsonToFile =
  (filePath, json) => fs.writeFileSync(filePath, JSON.stringify(json, null, 2));

module.exports = writeJsonToFile;
