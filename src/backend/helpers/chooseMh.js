const inquirer = require('inquirer');
const mhs = require('../../mhs.json');

const chooseMh = async () => {
  const { geeklistId } = await inquirer.prompt([
    {
      name: 'geeklistId',
      type: 'list',
      message: 'Choose MH: ',
      choices: mhs.all.reverse().map(mh => ({ value: mh.id, name: mh.name })),
    },
  ]);
  return geeklistId;
};

module.exports = chooseMh;
