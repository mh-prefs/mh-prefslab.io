const bggApi = require('./bggApi');

const CHUNK_SIZE = 20;

const getGameIds = (geeklist) => {
  const mainIds = geeklist.map(listItem => listItem.gameId);
  const fromComments = geeklist.map(listItem => listItem.subitems.map(subitem => subitem.gameId)).flat();
  return [...mainIds, ...fromComments];
};

const fetchGamesChunked = async function* fetchGamesChunked(geeklist) {
  const uniqueIds = Array.from(new Set(getGameIds(geeklist)));
  const chunkedIdLists = [];
  for (let i = 0; i < uniqueIds.length; i += CHUNK_SIZE) {
    chunkedIdLists.push(uniqueIds.slice(i, i + CHUNK_SIZE).reduce((prev, cur) => `${prev},${cur}`, '').substr(1));
  }

  // eslint-disable-next-line no-restricted-syntax
  for (const idList of chunkedIdLists) {
    yield bggApi('thing', { id: idList, stats: 1, versions: 1 });
  }
};

module.exports = async (geeklist) => {
  let games = [];
  // let flag = true;
  // eslint-disable-next-line semi
  for await (let response of fetchGamesChunked(geeklist)) {
    // eslint-disable-next-line no-unused-expressions, no-console
    // (flag && console.log(response.items.item[0]), flag = false);
    games = [...games, ...[response.items.item].flat()];
  }

  return games;
};
