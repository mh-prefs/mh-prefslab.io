const throwErrorLacking = (property) => { throw new Error(`Game has no ${property}`); };

const props = ['itemId', 'number', 'gameId', 'name', 'bggUser', 'id'];

const checkProperty = (game, propertyName) => game[propertyName] || throwErrorLacking(propertyName);

module.exports = (geekList) => {
  geekList.forEach((game) => {
    try {
      props.forEach(propName => checkProperty(game, propName));
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error(game, error);
    }
  });
};
