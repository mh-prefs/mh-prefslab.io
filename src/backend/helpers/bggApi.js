const axios = require('axios');
const Parser = require('xml2js-parser').Parser;

const xmlParser = new Parser({
  explicitArray: false,
  mergeAttrs: true,
});

const BGG_API_V1_BASE_URL = 'https://boardgamegeek.com/xmlapi/';
const BGG_API_V2_BASE_URL = 'https://boardgamegeek.com/xmlapi2/';
const RETRY_INTERVAL = 10000;

const getBaseUrlForEndpoint = endpoint => (/^geeklist/.test(endpoint) ? BGG_API_V1_BASE_URL : BGG_API_V2_BASE_URL);

const getBggApiCLient = (endpoint) => {
  const client = axios.create({
    baseURL: getBaseUrlForEndpoint(endpoint),
    timeout: 10000,
    headers: {
      // eslint-disable-next-line quote-props
      'Accept': 'text/xml',
      'Content-Type': 'text/xml',
    },
  });
  client.interceptors.response.use(response => xmlParser.parseString(response.data));
  return client;
};

// eslint-disable-next-line prefer-template
const createQueryString = params => (!params ? '' : ('?' + Object.entries(params).map(([key, val]) => `${key}=${val}`).join('&')));

const bggApi = async (endpoint, params) => {
  const client = getBggApiCLient(endpoint);
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < 10; i++) {
    try {
      // eslint-disable-next-line no-await-in-loop
      const response = await client.get(`${endpoint}${createQueryString(params)}`);
      if (response.message && response.message.includes('processed')) {
        throw new Error();
      }

      return response;
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err.message);
      // eslint-disable-next-line no-await-in-loop
      await new Promise(resolve => setTimeout(() => resolve(), RETRY_INTERVAL));
    }
  }

  throw new Error(`Max retries reached! Endpoint: ${endpoint}, Params: ${JSON.stringify(params)}`);
};

module.exports = bggApi;
